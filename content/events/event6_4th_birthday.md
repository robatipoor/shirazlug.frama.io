---
title : "چهارمین جشن تولد"
draft : false
description: "جشن تولد چهار سالگی شیرازلاگ"
author: "مریم بهزادی"
date: "1397-10-19"
summaryImage: "/img/events/4th-birthday.jpg"
---

[![4th-birthday](../../img/events/4th-birthday.jpg)](../../img/events/4th-birthday.jpg)

در صد و شصتمین نشست شیرازلاگ، بعد از ارائه مباحث مربوط به بینایی ماشین، تولد چهار سالگی شیرازلاگ توسط اعضاء جشن گرفته شد. توضیحات کامل تر این نشست را می توانید از طریق [این لینک](../../sessions/session160) دنبال کنید.