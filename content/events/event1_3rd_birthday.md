---
title: "سومین جشن تولد"
draft: false
description: "جشن تولد سه سالگی شیرازلاگ"
author: "مریم بهزادی"
date: "1396-10-18"
summaryImage: "/img/events/birthday.jpg"
---

[![birthday](../../img/events/birthday.jpg)](../../img/events/birthday.jpg)

نشست صد و سی شیرازلاگ مصادف شد با جشن تولد سه سالگی شیرازلاگ و به همین خاطر بخشی از وقت جلسه رو به برگزاری یه جشن ساده و خیلی کوچولو اختصاص دادیم. توضیحات مربوط به این نشست رو می تونید از طریق [این لینک](../../session/session130) دنبال کنید.